helpdir =:'/Users/joe/myStuff/dev/j/help/'

helphelp =: noun define
help 'this'
helplist ''
helpnew 'new-thing'
helpedit 'this'
)

help =: monad define
1!:1 < helpdir , y
)
NB. help ';'

helpnew =: monad define
a =. 'create new file and save as:'
a =. a ,: helpdir,y
)
NB. helpnew '/*'

helpedit =: monad define
open helpdir , y
)
NB. helpedit ';'

helplist =: monad define
items =. (1!:0 helpdir , '*')
0{"1 items
)
NB. helplist ''




NB. search parts of speech and personal libs
